// tslint:disable-next-line:max-line-length
import { TableCustomMenuRendererComponent } from 'app/pages/dashboard/pages/table-custom-menu-renderer/table-custom-menu-renderer.component';
// tslint:disable-next-line:max-line-length
import { TableCustomRendererComponent } from 'app/pages/dashboard/pages/table-custom-renderer/table-custom-renderer/table-custom-renderer.component';

export class AppConstants {
    public static themes = [
        {
            value: 'default',
            name: 'Light',
        },
        {
            value: 'dark',
            name: 'Dark',
        },
        {
            value: 'cosmic',
            name: 'Cosmic',
        },
        {
            value: 'corporate',
            name: 'Corporate',
        },
    ];
    public static defaultTheme: string = 'dark';
    public static menuItems = [
        {
            title: 'Dashboard',
            icon: 'home-outline',
            link: '/pages/dashboard',
            home: true,
        },
      //   {
      //     title: 'Reports',
      //     icon: 'grid-outline',
      //     children: [
      //         {
      //             title: 'Clients',
      //             link: 'pages/dashboard/details',
      //         },
      //         {
      //             title: 'Campaings',
      //             link: 'pages/dashboard/details/campaign-details',
      //         },
      //         {
      //           title: 'Ads',
      //           link: 'pages/dashboard/details/campaign-details/ad-details',
      //       },
      //     ],
      // },
        // {
        //     title: 'Charts',
        //     icon: 'pie-chart-outline',
        //     children: [
        //         {
        //             title: 'Pie',
        //             link: 'charts/pie',
        //         },
        //         {
        //             title: 'Bar',
        //             link: 'charts/bar',
        //         },
        //         {
        //             title: 'Line',
        //             link: 'charts/line',
        //         },
        //     ],
        // },
        // {
        //     title: 'Tables',
        //     icon: 'grid-outline',
        //     children: [
        //         {
        //             title: 'Table',
        //             link: 'table/table',
        //         },
        //         {
        //             title: 'Smart Table',
        //             link: 'table/smart-table',
        //         },
        //     ],
        // },
    ];
    public static LOGOUT_TITLE = 'Log out';
    public static PROFILE = 'Profile';
    public static DASHBOARD_VIEW = {
        Users: 'Users',
        Clients: 'Clients',
        ViewCount: 'View Count',
    };
    public static CLIENT_VIEW_TABLE_SETTINGS = {
        actions: {
          delete: false,
          add: false,
          edit: false,
      },
      pager: { display: false },
        columns: {
          sno: {
            title: '#',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value, row, cell) => {
              return cell.row.index + 1;
             },
          },
          clientName: {
            title: 'Client Name',
            type: 'string',
            filter: false,
          },
          state: {
            title: 'State',
            type: 'string',
            filter: false,
          },
          noOfUsersPlayed: {
            title: 'No Of Users Played',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value) => {
              return new Intl.NumberFormat().format(value);
            },
          },
          totalViewCount: {
            title: 'Total View Count (1 vc -> 3 secs)',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value) => {
              return new Intl.NumberFormat().format(value);
            },
          },
          wingaLeads: {
            title: 'WinGa Leads',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value) => {
              return new Intl.NumberFormat().format(value);
            },
          },
        },
      };

    public static CAMPAIGN_VIEW_TABLE_SETTINGS = {
        actions: {
          delete: false,
          add: false,
          edit: false,
      },
      pager: { display: false },
        columns: {
          sno: {
            title: '#',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value, row, cell) => {
              return cell.row.index + 1;
             },
          },
          campaignName: {
            title: 'Campaign Name',
            type: 'string',
            filter: false,
          },
          state: {
            title: 'State',
            type: 'string',
            filter: false,
          },
          noOfUsersPlayed: {
            title: 'No Of Users Played',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value) => {
              return new Intl.NumberFormat().format(value);
            },
          },
          totalEngagementTime: {
            title: 'Total Engagement Time (secs)',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value) => {
              return new Intl.NumberFormat().format(value);
            },
          },
          wingaLeads: {
            title: 'WinGa Leads',
            type: 'number',
            filter: false,
            valuePrepareFunction: (value) => {
              return new Intl.NumberFormat().format(value);
            },
          },
        },
      };
    public static AD_VIEW_TABLE_SETTINGS = {
      actions: {
        delete: false,
        add: false,
        edit: false,
    },
    pager: { display: false },
      columns: {
        sno: {
          title: '#',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value, row, cell) => {
            return cell.row.index + 1;
            },
        },
        adTitle: {
          title: 'Ad Title',
          type: 'string',
          filter: false,
        },
        state: {
          title: 'State',
          type: 'string',
          filter: false,
        },
        noOfUsersPlayed: {
          title: 'No Of Users Played',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value) => {
            return new Intl.NumberFormat().format(value);
          },
        },
        totalEngagementTime: {
          title: 'Total Engagement Time (secs)',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value) => {
            return new Intl.NumberFormat().format(value);
          },
        },
        wingaLeads: {
          title: 'WinGa Leads',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value) => {
            return new Intl.NumberFormat().format(value);
          },
        },
      },
    };
    public static USER_AD_VIEW_TABLE_SETTINGS = {
      actions: {
        columnTitle: '',
        delete: false,
        add: false,
        edit: false,
    },
    pager: { display: true, perPage: 20 },
      columns: {
        sno: {
          title: '#',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value, row, cell) => {
            return cell.row.index + 1;
            },
        },
        userName: {
          title: 'User Name',
          type: 'string',
          filter: false,
        },
        userMobile: {
          title: 'Mobile Number',
          type: 'string',
          filter: false,
        },
        playedTime: {
          title: 'Played Time',
          type: 'string',
          filter: false,
        },
        name: {
          type: 'custom',
          filter: false,
          renderComponent: TableCustomRendererComponent,
        },
      },
    };
    public static SHORT_SESSIONS_TABLE_SETTINGS = {
      actions: {
        delete: false,
        add: false,
        edit: false,
    },
    pager: { display: true, perPage: 20 },
      columns: {
        sno: {
          title: '#',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value, row, cell) => {
            return cell.row.index + 1;
            },
        },
        name: {
          title: 'Name',
          type: 'string',
          filter: false,
        },
        noOfUsersParticipated: {
          title: 'Users Participated',
          type: 'string',
          filter: false,
          valuePrepareFunction: (value) => {
            return new Intl.NumberFormat().format(value);
          },
        },
        noOfWinners: {
          title: 'Users Played',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value) => {
            return new Intl.NumberFormat().format(value);
          },
        },
        conductedTime: {
          title: 'Conducted Time',
          type: 'number',
          filter: false,
        },
        cgsId: {
          type: 'custom',
          filter: false,
          renderComponent: TableCustomMenuRendererComponent,
        },
      },
    };
    public static SHORT_SESSIONS_WINNERS_DETAILS_TABLE_SETTINGS = {
      actions: {
        delete: false,
        add: false,
        edit: false,
    },
    pager: { display: true, perPage: 20 },
      columns: {
        sno: {
          title: '#',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value, row, cell) => {
            return cell.row.index + 1;
            },
        },
        userName: {
          title: 'Name',
          type: 'string',
          filter: false,
        },
        mobileNumber: {
          title: 'Mobile Number',
          type: 'string',
          filter: false,
        },
        createdTime: {
          title: 'Created Time',
          type: 'string',
          filter: false,
        },
      },
    };
    public static SHORT_SESSIONS_PARTICIPATED_USERS_TABLE_SETTINGS = {
      actions: {
        delete: false,
        add: false,
        edit: false,
    },
    pager: { display: true, perPage: 20 },
      columns: {
        sno: {
          title: '#',
          type: 'number',
          filter: false,
          valuePrepareFunction: (value, row, cell) => {
            return cell.row.index + 1;
            },
        },
        userName: {
          title: 'Name',
          type: 'string',
          filter: false,
        },
        mobileNumber: {
          title: 'Mobile Number',
          type: 'string',
          filter: false,
        },
        createdTime: {
          title: 'Created Time',
          type: 'string',
          filter: false,
        },
      },
    };
    public static YEAR_DROP_DOWN = [
      {
        'month': 11,
        'lable': 'November 2021',
        'fromDate': '2021-11-01',
        'toDate': '2021-11-30',
      },
      {
        'month': 10,
        'lable': 'October 2021',
        'fromDate': '2021-10-01',
        'toDate': '2021-10-31',
      },
      {
        'month': 9,
        'lable': 'September 2021',
        'fromDate': '2021-09-01',
        'toDate': '2021-09-30',
      },
      {
        'month': 8,
        'lable': 'August 2021',
        'fromDate': '2021-08-01',
        'toDate': '2021-08-31',
      },
      {
        'month': 7,
        'lable': 'July 2021',
        'fromDate': '2021-07-01',
        'toDate': '2021-07-31',
      },
      {
        'month': 6,
        'lable': 'June 2021',
        'fromDate': '2021-06-01',
        'toDate': '2021-06-30',
      },
      {
        'month': 5,
        'lable': 'May 2021',
        'fromDate': '2021-05-01',
        'toDate': '2021-05-31',
      },
    ];
    public static SS_MENU_ITEM_WINNERS = 'Winners';
    public static SS_MENU_ITEM_PARTICIPATED_USERS = 'Participated Users';
}
