/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppStore } from './@core/store/app-store.service';
import { AnalyticsService } from './@core/utils/analytics.service';
import { SeoService } from './@core/utils/seo.service';

@Component({
  selector: 'winga-app',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent implements OnInit {

  constructor(private analytics: AnalyticsService, private seoService: SeoService, router: Router, private appStore: AppStore) {
    if ( localStorage.getItem('user')  === 'undefined' || localStorage.getItem('user')  === null) {
      router.navigate(['pages/login']);
    } else {
      this.appStore.user = JSON.parse(localStorage.getItem('user')).user;
    }
  }
  ngOnInit() {
    this.analytics.trackPageViews();
    this.seoService.trackCanonicalChanges();
  }
}
