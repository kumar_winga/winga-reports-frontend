import { Component, OnInit } from '@angular/core';
import { DashboardStore } from '../store/dashboard-store.service';

@Component({
  selector: 'winga-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.scss'],
})
export class DashboardMainComponent implements OnInit {

  constructor(public dashboardStore: DashboardStore) {
    this.dashboardStore.getDashboardTiles();
   }

  ngOnInit(): void {}

}
