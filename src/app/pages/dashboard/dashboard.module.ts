import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ChartModule } from 'angular2-chartjs';
import { NgxEchartsModule } from 'ngx-echarts';
import { NbCardModule,
         NbButtonModule,
         NbIconModule,
         NbActionsModule,
         NbCheckboxModule,
         NbDatepickerModule,
         NbInputModule,
         NbRadioModule,
         NbSpinnerModule,
         NbContextMenuModule,
         NbSelectModule } from '@nebular/theme';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { DashboardMainComponent } from './main/dashboard-main.component';
import { DashboardFilterComponent } from './pages/dashboard-filter/dashboard-filter.component';
import { DashboardComponent } from './pages/dashboard-tiles/dashboard.component';
import { DashboardStore } from './store/dashboard-store.service';
import { DashboardService } from './services/dashboard.service';
import { NbMomentDateModule } from '@nebular/moment';
import { CampaignDetailsComponent } from './pages/campaign-details/campaign-details/campaign-details.component';
import { AdDetailsComponent } from './pages/ad-details/ad-details/ad-details.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { ShortSessionDetailsComponent } from './pages/short-session-details/short-session-details/short-session-details.component';
import { UserContentViewsComponent } from './pages/user-content-views/user-content-views/user-content-views.component';
import { TableCustomRendererComponent } from './pages/table-custom-renderer/table-custom-renderer/table-custom-renderer.component';
import { UnmaskDetailsModalComponent } from './pages/unmask-details-modal/unmask-details-modal/unmask-details-modal.component';
import { TableCustomMenuRendererComponent } from './pages/table-custom-menu-renderer/table-custom-menu-renderer.component';
import { WinnerDetailsComponent } from './pages/winner-details/winner-details.component';
import { ParticipatedUsersComponent } from './pages/participated-users/participated-users.component';

const routes: Routes = [{
  path: '',
  component: DashboardMainComponent,
  children: [
    {
        path: 'details',
        component: DashboardFilterComponent,
    },
    {
      path: 'details/campaign-details',
      component: CampaignDetailsComponent,
    },
    {
      path: 'details/campaign-details/ad-details',
      component: AdDetailsComponent,
    },
    {
      path: 'details/campaign-details/ad-details/short-session-details',
      component: ShortSessionDetailsComponent,
    },
    {
      path: 'details/campaign-details/ad-details/short-session-details/winner-details',
      component: WinnerDetailsComponent,
    },
    {
      path: 'details/campaign-details/ad-details/short-session-details/participated-users',
      component: ParticipatedUsersComponent,
    },
    {
      path: 'details/campaign-details/ad-details/user-content-views',
      component: UserContentViewsComponent,
    },
    {
        path: '',
        component: DashboardComponent,
    },
  ]},
];

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NbCardModule,
    ThemeModule,
    NbButtonModule,
    NbIconModule,
    NbActionsModule,
    NbButtonModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbSpinnerModule,
    NbMomentDateModule,
    ChartModule,
    NgxEchartsModule,
    NgxChartsModule,
    NbContextMenuModule,
    RouterModule.forChild(routes),
  ],
  declarations: [
    DashboardComponent,
    DashboardFilterComponent,
    DashboardMainComponent,
    CampaignDetailsComponent,
    AdDetailsComponent,
    ShortSessionDetailsComponent,
    UserContentViewsComponent,
    TableCustomRendererComponent,
    UnmaskDetailsModalComponent,
    TableCustomMenuRendererComponent,
    WinnerDetailsComponent,
    ParticipatedUsersComponent,
  ],
  providers: [DashboardStore, DashboardService],
})
export class DashboardModule { }
