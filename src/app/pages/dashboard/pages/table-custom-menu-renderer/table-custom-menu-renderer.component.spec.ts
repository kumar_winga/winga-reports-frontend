import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableCustomMenuRendererComponent } from './table-custom-menu-renderer.component';

describe('TableCustomMenuRendererComponent', () => {
  let component: TableCustomMenuRendererComponent;
  let fixture: ComponentFixture<TableCustomMenuRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TableCustomMenuRendererComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableCustomMenuRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
