import { Component, OnInit } from '@angular/core';
import { AppConstants } from 'app/util/app-constants';
import { DashboardStore } from '../../store/dashboard-store.service';

@Component({
  selector: 'winga-participated-users',
  templateUrl: './participated-users.component.html',
  styleUrls: ['./participated-users.component.scss'],
})
export class ParticipatedUsersComponent implements OnInit {
  settings = AppConstants.SHORT_SESSIONS_PARTICIPATED_USERS_TABLE_SETTINGS;
  constructor(public dashboardStore: DashboardStore) {
    this.loadData();
  }
  ngOnInit(): void {
  }
  loadData() {
    this.dashboardStore.getShortSessionParticipatedUsers();
  }
  rowClicked(event) {
  }

}
