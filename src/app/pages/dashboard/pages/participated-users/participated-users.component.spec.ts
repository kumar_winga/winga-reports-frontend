import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipatedUsersComponent } from './participated-users.component';

describe('ParticipatedUsersComponent', () => {
  let component: ParticipatedUsersComponent;
  let fixture: ComponentFixture<ParticipatedUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipatedUsersComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipatedUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
