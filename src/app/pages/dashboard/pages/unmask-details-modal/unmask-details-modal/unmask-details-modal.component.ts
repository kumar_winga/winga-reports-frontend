import { Component, OnInit } from '@angular/core';
import { AppStore } from 'app/@core/store/app-store.service';
import { DashboardStore } from 'app/pages/dashboard/store/dashboard-store.service';

@Component({
  selector: 'winga-unmask-details-modal',
  templateUrl: './unmask-details-modal.component.html',
  styleUrls: ['./unmask-details-modal.component.scss'],
})
export class UnmaskDetailsModalComponent implements OnInit {

  constructor(public appStore: AppStore, public dashboardStore: DashboardStore) { }

  ngOnInit(): void {
  }

}
