import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConstants } from 'app/util/app-constants';
import { Util } from 'app/util/util';
import { LocalDataSource } from 'ng2-smart-table';
import { DashboardStore } from '../../store/dashboard-store.service';

@Component({
  selector: 'winga-dashboard-filter',
  templateUrl: './dashboard-filter.component.html',
  styleUrls: ['./dashboard-filter.component.scss'],
})
export class DashboardFilterComponent implements OnInit {
  appConstants = AppConstants;
  util = Util;
  source: LocalDataSource = new LocalDataSource();
  settings = AppConstants.CLIENT_VIEW_TABLE_SETTINGS;
  constructor(public dashboardStore: DashboardStore, private router: Router, private activatedRoute: ActivatedRoute) {
    this.dashboardStore.clientData = null;
    this.dashboardStore.fromDate = new Date();
    this.dashboardStore.toDate = new Date();
   }

  ngOnInit(): void {
  }
  loadData() {
    this.dashboardStore.getClientData();
  }
  rowClicked(event) {
    const rowData = event.data;
    this.dashboardStore.clientId = rowData.clientId;
    this.router.navigate(['campaign-details'], {relativeTo: this.activatedRoute});

  }
}
