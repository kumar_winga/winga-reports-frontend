import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WinnerDetailsComponent } from './winner-details.component';

describe('WinnerDetailsComponent', () => {
  let component: WinnerDetailsComponent;
  let fixture: ComponentFixture<WinnerDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WinnerDetailsComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WinnerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
