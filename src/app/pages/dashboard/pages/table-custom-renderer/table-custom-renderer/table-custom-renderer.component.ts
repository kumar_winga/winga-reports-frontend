import { Component, Input } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import { DashboardStore } from 'app/pages/dashboard/store/dashboard-store.service';
import { AppStore } from 'app/@core/store/app-store.service';

@Component({
  selector: 'winga-table-custom-renderer',
  templateUrl: './table-custom-renderer.component.html',
  styleUrls: ['./table-custom-renderer.component.scss'],
})
export class TableCustomRendererComponent implements ViewCell  {
  @Input() value: any;    // This hold the cell value
  @Input() rowData: any;  // This holds the entire row object
  constructor(public dashboardStore: DashboardStore, private appStore: AppStore) {

  }
  unmask() {
    this.dashboardStore.selectedUserAdViewRow = this.rowData;
    this.appStore.selectedUserAdViewRow = this.rowData;
    this.dashboardStore.getUnmaskData();
  }
}
