import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShortSessionDetailsComponent } from './short-session-details.component';

describe('ShortSessionDetailsComponent', () => {
  let component: ShortSessionDetailsComponent;
  let fixture: ComponentFixture<ShortSessionDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShortSessionDetailsComponent ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShortSessionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
