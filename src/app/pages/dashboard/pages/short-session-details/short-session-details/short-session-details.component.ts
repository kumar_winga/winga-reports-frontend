import { Component, OnInit } from '@angular/core';
import { DashboardStore } from 'app/pages/dashboard/store/dashboard-store.service';
import { AppConstants } from 'app/util/app-constants';
import { Util } from 'app/util/util';

@Component({
  selector: 'winga-short-session-details',
  templateUrl: './short-session-details.component.html',
  styleUrls: ['./short-session-details.component.scss'],
})
export class ShortSessionDetailsComponent implements OnInit {

  appConstants = AppConstants;
  util = Util;
  settings = AppConstants.SHORT_SESSIONS_TABLE_SETTINGS;
  constructor(public dashboardStore: DashboardStore) {
    this.loadData();
   }

  ngOnInit(): void {
  }
  loadData() {
    this.dashboardStore.getShortSessions();
  }
  rowClicked(event) {
  }
  // this.router.navigate(['ad-details'], {relativeTo: this.activatedRoute});
}
