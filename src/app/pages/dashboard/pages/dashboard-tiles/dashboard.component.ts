import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NbThemeService, NbColorHelper } from '@nebular/theme';
import { DashboardStore } from '../../store/dashboard-store.service';

@Component({
  selector: 'winga-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnDestroy {
  results = [
    { name: 'Germany', value: 8940 },
    { name: 'USA', value: 5000 },
    { name: 'France', value: 7200 },
  ];
  showLegend = true;
  showXAxis = true;
  showYAxis = true;
  xAxisLabel = 'Country';
  yAxisLabel = 'Population';
  colorScheme: any;
  themeSubscription: any;
  wingaFacts = [
    'We crossed 3.5 Lakhs WinGa users',
    'Daily active users are 1.4 Lakhs',
    'Short sessions for mobile phones - converted 12 leads for iPhone 12 Pro',
  ];
  constructor(private router: Router, private activatedRoute: ActivatedRoute,
              public dashboardStore: DashboardStore, private theme: NbThemeService) {
      this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
        const colors: any = config.variables;
        const echarts: any = config.variables.echarts;
        const chartjs: any = config.variables.chartjs;
        this.colorScheme = {
          domain: [colors.primaryLight, colors.infoLight, colors.successLight, colors.warningLight, colors.dangerLight],
        };
    });
  }
  viewDetails(view) {
    this.dashboardStore.loadViewDetails(view);
    this.dashboardStore.resetValues();
    switch (view) {
      case 'Clients' : this.router.navigate(['details'], {relativeTo: this.activatedRoute}); break;
      case 'Campaigns' : this.router.navigate(['details/campaign-details'], {relativeTo: this.activatedRoute}); break;
      case 'Ad View Count' : this.router.navigate(['details/campaign-details/ad-details'], {relativeTo: this.activatedRoute}); break;
      case 'Users' : this.router.navigate(['details/campaign-details/ad-details/user-content-views'],
                    {relativeTo: this.activatedRoute}); break;
      case 'Short Sessions' : this.router.navigate(['details/campaign-details/ad-details/short-session-details'],
                              {relativeTo: this.activatedRoute}); break;
    }
  }
  ngOnDestroy(): void {
    this.themeSubscription.unsubscribe();
  }
}
