import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppStore } from 'app/@core/store/app-store.service';
import { DashboardService } from '../services/dashboard.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Util } from 'app/util/util';
import { AppConstants } from 'app/util/app-constants';
import { environment } from 'environments/environment';
import { UnmaskDetailsModalComponent } from '../pages/unmask-details-modal/unmask-details-modal/unmask-details-modal.component';
import { NbWindowService } from '@nebular/theme';

@Injectable({
  providedIn: 'root',
})
export class DashboardStore {
  apiURL = environment.apiURL;
  util = Util;
  dashboardTiles = null;
  states: any = null;
  clients: any = null;
  campaignGroups: any = null;
  campaigns: any = null;
  clientData: any = null;
  campaignsData: any = null;
  adsData: any = null;
  userAdViewData: any = null;
  unmaskData: any = null;
  selectedUserAdViewRow: any = null;
  clientId: any = null;
  campaignId: any = null;
  adsList: any = null;
  adId: any = null;
  fromDate: any = null;
  toDate: any = null;
  dateRange: any = null;

  shortSessions: any = null;
  ss_cgsId: any = null;
  shortSessionWinners: any = null;
  shortSessionParticipatedUsers: any = null;


  showSpinner: boolean = false;
  dateRangeDropDown = AppConstants.YEAR_DROP_DOWN;
  totals = {
    noOfUsersPlayed: 0,
    totalEngagementTime: 0,
    wingaLeads: 0,
  };

  currentDashboardView = null;

  selectedClient: any = null;
  selectedCampaignGroup: any = null;
  constructor(private http: HttpClient, private appStore: AppStore, private dashboardService: DashboardService,
    private router: Router, private activatedRoute: ActivatedRoute, private windowService: NbWindowService ) {
  }

  resetValues() {
    this.clientId = null;
    this.campaignId = null;
    this.adId = null;
    this.dateRange = null;
    this.clientData = null;
    this.adsData = null;
    this.campaignsData = null;
    this.userAdViewData = null;
  }

  getStates() {
    this.dashboardService.getStates().subscribe( states => {
      this.states = states;
    });
  }
  getClients() {
    this.dashboardService.getClients().subscribe( clients => {
      this.clients = clients;
    });
  }
  getCampaignGroups() {
    this.dashboardService.getCampaignGroups(this.clientId).subscribe( campaignGroups => {
      this.campaignGroups = campaignGroups;
    });
  }
  getCampaigns() {
    this.dashboardService.getCampaigns(this.clientId).subscribe( campaigns => {
      this.campaigns = campaigns;
    });
  }
  getDashboardTiles() {
    this.dashboardService.getDashboardTiles().subscribe( tiles => {
      this.dashboardTiles = tiles;
    });
  }
  getCampaignsData() {
    this.getFromAndToDate();
    this.dashboardService.getCampaignsData(this.clientId, this.fromDate, this.toDate).subscribe( campaigns => {
      this.campaignsData = campaigns;
    });
  }
  getAdData() {
    this.showSpinner = true;
    this.getFromAndToDate();
    this.dashboardService.getAdData(this.clientId, this.campaignId, this.fromDate, this.toDate).subscribe( campaigns => {
      this.adsData = campaigns;
      this.showSpinner = false;
    }, error => {
      this.showSpinner = false;
    });
  }
getUserContentViewsData() {
    this.showSpinner = true;
    this.getFromAndToDate();
    this.dashboardService.getUserContentViewsData(this.adId, this.fromDate, this.toDate).subscribe( userAdViewData => {
      this.userAdViewData = userAdViewData;
      this.showSpinner = false;
    }, error => {
      this.showSpinner = false;
    });
  }
  loadViewDetails(view) {
    this.currentDashboardView = view;
  }
  getClientData() {
    this.getFromAndToDate();
    this.dashboardService.getClientData(this.fromDate, this.toDate).subscribe( data => {
      this.clientData = data;
      this.getTotals();
    });
  }
  getTotals() {
    for (const item of this.clientData.clientViewData) {
      this.totals.noOfUsersPlayed += item.noOfUsersPlayed;
      this.totals.totalEngagementTime += item.totalEngagementTime;
      this.totals.wingaLeads += item.wingaLeads;
  }
  }

  getFromAndToDate() {
    this.dateRangeDropDown.forEach( element => {
      if (element.month === this.dateRange) {
        this.fromDate = this.util.formatDate(element.fromDate);
        this.toDate = this.util.formatDate(element.toDate);
      }
    });
  }
  getContentsByCampaignId() {
    this.dashboardService.getContentsByCampaignId(
      this.campaignId).subscribe( data => {
        this.adsList = data;
  });
  }
  getUnmaskData() {
    this.dashboardService.getUnmaskUserMobile(
        this.selectedUserAdViewRow.userId,
        this.appStore.user.cpId,
        this.appStore.user.username,
        this.fromDate,
        this.toDate).subscribe( data => {
      this.unmaskData = data;
      this.appStore.unmaskData = data;
      this.windowService.open(UnmaskDetailsModalComponent, { title: `User Details`, windowClass: 'profile-window' });
    });
  }
  getShortSessions() {
    this.showSpinner = true;
    this.dashboardService.getShortSessions().subscribe( data => {
        this.shortSessions = data;
        this.showSpinner = false;
    });
  }
  getShortSessionWinners() {
    this.showSpinner = true;
    this.dashboardService.getShortSessionWinners(this.ss_cgsId).subscribe( data => {
        this.shortSessionWinners = data;
        this.showSpinner = false;
    });
  }
  getShortSessionParticipatedUsers() {
    this.showSpinner = true;
    this.dashboardService.getShortSessionParticipatedUsers(this.ss_cgsId).subscribe( data => {
        this.shortSessionParticipatedUsers = data;
        this.showSpinner = false;
    });
  }
}
