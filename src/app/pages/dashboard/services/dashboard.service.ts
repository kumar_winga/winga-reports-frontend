import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppStore } from 'app/@core/store/app-store.service';
import { environment } from 'environments/environment';
@Injectable({
  providedIn: 'root',
})
export class DashboardService {
  apiURL = environment.apiURL;
  constructor(private http: HttpClient, private appStore: AppStore, private router: Router) { }

  getDashboardTiles() {
    return this.http.get<any>(`${this.apiURL}/report/dashboard`);
  }
  getStates() {
    return this.http.get<any>(`${this.apiURL}/meta/states`);
  }
  getClients() {
    return this.http.get<any>(`${this.apiURL}/meta/clients`);
  }
  getCampaignGroups(clientId) {
    return this.http.get<any>(`${this.apiURL}/meta/campaignGroups/${clientId}`);
  }
  getClientData(fr, to) {
    return this.http.get<any>(`${this.apiURL}/report/clientViewData?fromDate=${fr}&toDate=${to}`);
  }
  getCampaigns(clientID) {
    return this.http.get<any>(`${this.apiURL}/meta/campaigns/client/${clientID}`);
  }
  getCampaignsData(clientID, fr, to) {
    return this.http.get<any>(`${this.apiURL}/report/campaignViewData?clientId=${clientID}&fromDate=${fr}&toDate=${to}`);
  }
  getAdData(clientID, campaignId, fr, to) {
    return this.http.get<any>(`${this.apiURL}/report/adViewData?clientId=${clientID}&campaignId=${campaignId}&fromDate=${fr}&toDate=${to}`);
  }
  getUserContentViewsData(adID, fr, to) {
    return this.http.get<any>(`${this.apiURL}/report/userAdViewData?adId=${adID}&fromDate=${fr}&toDate=${to}`);
  }
  getUnmaskUserMobile(userId, cpId, username, fr, to) {
    return this.http.get<any>(`${this.apiURL}/report/unMaskedUserMobile?userId=${userId}&clientUserId=1&cpId=${cpId}&username=${username}&fromDate=${fr}&toDate=${to}`);
  }
  getContentsByCampaignId(campaignId) {
    return this.http.get<any>(`${this.apiURL}/meta/contents/campaign/${campaignId}`);
  }
  getShortSessions() {
    return this.http.get<any>(`${this.apiURL}/report/shortSessionDetails`);
  }
  getShortSessionWinners(cgsId) {
    return this.http.get<any>(`${this.apiURL}/report/shortSessionWinner/${cgsId}`);
  }
  getShortSessionParticipatedUsers(cgsId) {
    return this.http.get<any>(`${this.apiURL}/report/shortSessionParticipatedUsers/${cgsId}`);
  }
}
