import { NbMenuItem } from '@nebular/theme';
import { AppConstants } from 'app/util/app-constants';

export const MENU_ITEMS: NbMenuItem[] = AppConstants.menuItems;
