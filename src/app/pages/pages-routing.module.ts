import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      loadChildren: () => import ('../pages/dashboard/dashboard.module').then(m => m.DashboardModule),
    },
    {
      path: 'charts',
      loadChildren: () => import ('../features/charts/charts.module').then(m => m.ChartsModule),
    },
    {
      path: 'table',
      loadChildren: () => import ('../features/table/tables.module').then(m => m.TablesModule),
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
