import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppStore } from 'app/@core/store/app-store.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class InterceptorService implements HttpInterceptor {
  constructor(public appStore: AppStore) {
  }
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = JSON.parse(localStorage.getItem('user'));
    let authReq = null;
    if (token) {
      const tokenString = 'Bearer ' + token.tokens['access-token'];
      authReq = req.clone({
        headers: new HttpHeaders({
          'Authorization': tokenString,
        }),
      });
    } else  {
      // Only for Login we dont require Authorization in header
      authReq = req.clone({
        headers: new HttpHeaders({
        }),
      });
    }
    return next.handle(authReq).pipe(
      map((event) => {
        return event;
      }),
    );
  }
}
