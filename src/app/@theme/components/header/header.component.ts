import { Component, OnDestroy, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NbMediaBreakpointsService, NbMenuService, NbSidebarService, NbThemeService, NbWindowService } from '@nebular/theme';

import { UserData } from '../../../@core/data/users';
import { map, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { AppConstants } from 'app/util/app-constants';
import { AppStore } from 'app/@core/store/app-store.service';
import { LoginService } from 'app/login/login.service';
import { UserProfileComponent } from '../user-profile/user-profile/user-profile.component';

@Component({
  selector: 'winga-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html',
})
export class HeaderComponent implements OnInit, OnDestroy {
  @ViewChild('contentTemplate', { static: true }) contentTemplate: TemplateRef<any>;
  private destroy$: Subject<void> = new Subject<void>();
  userPictureOnly: boolean = false;
  user: any;

  themes = AppConstants.themes;

  currentTheme = 'default';

  userMenu = [{ title: AppConstants.PROFILE }, { title: AppConstants.LOGOUT_TITLE }];

  constructor(private sidebarService: NbSidebarService,
    private menuService: NbMenuService,
    private themeService: NbThemeService,
    private userService: UserData,
    private breakpointService: NbMediaBreakpointsService,
    public appStore: AppStore,
    public loginService: LoginService,
    private windowService: NbWindowService) {
  }

  ngOnInit() {
    this.currentTheme = this.themeService.currentTheme;
    this.user = { name: '', picture: 'assets/images/user-pic-outline.png' };
    this.userService.getUsers()
      .pipe(takeUntil(this.destroy$))
      .subscribe((users: any) => {
        // this.user = users.nick
      });
    // this.user = this.appStore.user;

    const { xl } = this.breakpointService.getBreakpointsMap();
    this.themeService.onMediaQueryChange()
      .pipe(
        map(([, currentBreakpoint]) => currentBreakpoint.width < xl),
        takeUntil(this.destroy$),
      )
      .subscribe((isLessThanXl: boolean) => this.userPictureOnly = isLessThanXl);

    this.themeService.onThemeChange()
      .pipe(
        map(({ name }) => name),
        takeUntil(this.destroy$),
      )
      .subscribe(themeName => {
        this.currentTheme = themeName; localStorage.setItem('theme', themeName);
        this.appStore.theme = themeName;
      });
      this.menuService.onItemClick().subscribe(( event ) => {
        // this.onItemSelection(event.item.title);
      if ( event.item.title === AppConstants.LOGOUT_TITLE ) {
          this.loginService.logout();
        }
        if ( event.item.title === AppConstants.PROFILE ) {
          const buttonsConfig = {
            minimize: false,
            maximize: false,
            fullScreen: true,
          };
          this.windowService.open(UserProfileComponent, { title: `User Profile`, windowClass: 'profile-window' });
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }

  changeTheme(themeName: string) {
    this.themeService.changeTheme(themeName);
  }

  toggleSidebar(): boolean {
    this.sidebarService.toggle(true, 'menu-sidebar');

    return false;
  }

  navigateHome() {
    // this.menuService.navigateHome();
    // return false;
  }
}
