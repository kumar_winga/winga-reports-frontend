import { NgModule } from '@angular/core';
import { NgxEchartsModule } from 'ngx-echarts';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NbCardModule } from '@nebular/theme';
import { ChartModule } from 'angular2-chartjs';
import { ThemeModule } from '../../@theme/theme.module';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ChartsMainComponent } from './main/charts-main/charts-main.component';
import { PieChartComponent } from './pages/pie-chart/pie-chart.component';
import { BarChartComponent } from './pages/bar-chart/bar-chart.component';
import { LineChartComponent } from './pages/line-chart/line-chart.component';

const routes: Routes = [{
  path: '',
  component: ChartsMainComponent,
  },
  {
    path: 'pie',
    component: PieChartComponent,
  },
  {
    path: 'bar',
    component: BarChartComponent,
  },
  {
    path: 'line',
    component: LineChartComponent,
  },
];


@NgModule({
  declarations: [ChartsMainComponent, PieChartComponent, BarChartComponent, LineChartComponent],
  imports: [
    CommonModule,
    NgxEchartsModule,
    NgxChartsModule,
    NbCardModule,
    ThemeModule,
    ChartModule,
    RouterModule.forChild(routes),
  ],
})
export class ChartsModule { }
