import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ThemeModule } from './../@theme/theme.module';
import { FormsModule } from '@angular/forms';
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbUserModule,
  NbSpinnerModule,
  NbLayoutModule,
} from '@nebular/theme';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login.component';
import { LoginService } from './login.service';

const routes: Routes = [{
  path: '',
  component: LoginComponent,
  },
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    NbActionsModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule,
    NbDatepickerModule, NbIconModule,
    NbInputModule,
    NbRadioModule,
    NbSelectModule,
    NbUserModule,
    ThemeModule,
    NbSpinnerModule,
    NbLayoutModule,
    FormsModule,
    RouterModule.forChild(routes),
  ],
  providers: [LoginService],
})
export class LoginModule { }
